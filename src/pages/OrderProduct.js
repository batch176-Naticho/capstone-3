
import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams, Link, useNavigate } from 'react-router-dom';
//useParams() contains any values we are trying to pass in the URL stored
//useParams is how we receive the courseId passed via the URL

export default function OrderProduct() {

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);


	useEffect(() => {

		fetch(`http://localhost:4000/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setPrice(data.quantity)
		})

	}, [])

	const { user } = useContext(UserContext);


	//enroll function
	const order = (productId) => {

		fetch('http://localhost:4000/orders/createOrder', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: 'Successfully enrolled!',
					icon: 'success',
					text: `You have successfully enrolled for this ${ name }`
				})

				navigate('/products')
			} else {
				Swal.fire({
					title: 'error!',
					icon: 'error',
					text: 'Something went wrong, please try again'
				})
			}
		})
	}

	return(
		<Container>
			<Card>
				<Card.Header>
					<h4>{ name }</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{ description }</Card.Text>
					<h6>Price: Php { price } </h6>
					<Button>+</Button>
					<input type="number" value="0"></input>
 					 <Button>-</Button>
				</Card.Body>

				<Card.Footer>
				{ user.accessToken !== null ?
					<Button variant="primary" onClick={() => order(productId)}>Buy</Button>
					:
					<Button variant="warning" as={ Link } to="/login">Login to Enroll</Button>
				 }
					
					
				</Card.Footer>
			</Card>
		</Container>

		)
}