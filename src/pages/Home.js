import HomePage from '../components/HomePage';
import Highlight from '../components/Highlight';
import { Container } from 'react-bootstrap';


export default function Home() {
	return(
	<>
		<HomePage />
		<Container>
			<Highlight />		
		</Container>
	</>
	)
}