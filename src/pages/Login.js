import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';


export default function Login() {

	const navigate = useNavigate();

	const  { user, setUser } = useContext(UserContext)	

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	
	const [ isActive, setIsActive ] = useState(true);

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

//when login button is triggered it direct here
	function authentication(e) {
		e.preventDefault();
//trigers the fetch and go to the url to connect to the backend (login)
		fetch ('http://localhost:4000/users/login', {
			method: 'POST',
//when we pass data we set the content type for it to be passed, we only only pass file in json format (JSON.stringify)
			headers: {'Content-Type': 'Application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
//parse the stringified JSON for the client to understand
		.then(res => res.json())
//receive the data (accessToken)
		.then(data => {
			console.log(data)

//if accessToken is not empty
			if(data.accessToken !== undefined){
//it will be saved in the localStorage
				localStorage.setItem('accessToken', data.accessToken);
//and also save it in the context of the global state (to be accessible in the other files)
				setUser({
					accessToken: data.accessToken
				})
				Swal.fire({
					title: 'Welcome',
					icon: 'success',
					text: 'You are now login'
				})

//if token is verified the we will get the details of the user
//no method: 'GET' beacause in fetch the default method is get
//fetch the details of the user via the url that connects to the backend
				fetch('http://localhost:4000/users/details', {
					headers: {
//in the backend we reference the authorization to get the details of the user
//Authorization will check the accessToken pass to it upon login and Checks if the client is authorize to get the details of the user
						Authorization: `Bearer ${data.accessToken}`
					}
				})
//upon receiving the response, it will be parse so the client will understand
				.then(res => res.json())
				.then(data => {
// data of the user who login will appear in the console
					console.log(data)

//when the data receive is a data of an admin (isAdmin=true)
					if(data.isAdmin === true ) {
//the data will be saved in the localStorage and the global context
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							isAdmin: data.isAdmin
						})
//and will navigate to the  products page
						navigate('/products')
					}else{
//if not admin go to homepage
						navigate('/products')
					}
				})
			}else{
				Swal.fire({
					title: 'Ooooops',
					icon: 'error',
					text: 'Invalid email or password'
				})
			}
//to clear after login
			setEmail('')
			setPassword('')
		})

	}			
				
	return(

		(user.accessToken !== null) ?

		<Navigate to="/products" />
		:
		<Form onSubmit={e => authentication(e)}>
            <h1>Login</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				    type="email"
				    placeholder="Enter email"
				    required
				    value={email}
				    onChange={e => setEmail(e.target.value)}
				    />
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				    type="password"
				    placeholder="Enter your Password"
				    required
				    value={password}
				    onChange={e => setPassword(e.target.value)}
				    />
			</Form.Group>
			{ isActive ?
			<Button variant="primary" type="submit" className="mt-3">
				Login
			</Button>
			:
			<Button variant="primary" type="submit" className="mt-3" disabled>
				Login
			</Button>
			}
		</Form>


		)
}