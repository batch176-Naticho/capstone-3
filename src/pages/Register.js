import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';



export default function Register() {

	const navigate = useNavigate();

	const { user, setUser } = useContext(UserContext)

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');

	//Validation
	const [ isActive, setIsActive ] = useState(true)

	useEffect(() => {
		if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}	
	}, [ email, password, verifyPassword])

	function registerUser(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/register',{
			method:'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				password: password,
				verifyPassword: verifyPassword
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			
				if(data.email !== ''){
					localStorage.setItem('email', data.email);
					setUser({
						email:data.email
					})

					Swal.fire({
						title:'Congrats!',
						icon:'success',
						text:'You are now Registered'
					})
					navigate('/login')
				}else{
					Swal.fire({
						title:'Oooops',
						icon:'error',
						text:'Failed to Register'
					})
				};

			setEmail('')
			setPassword('')
			setVerifyPassword('')
		})

	}


	return(

		(user.accessToken !== null) ?

		<Navigate to="/login"/>
		:
		<Form onSubmit={e => registerUser(e)} className="registerForm">
		<h1 className="mt-3">Register</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				type="email"
				placeholder="Enter your email" 
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We will never share your email.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Enter your password"
				required
				value={password}
				onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
				type="password"
				placeholder="Verify password"
				required
				value={verifyPassword}
				onChange={e => setVerifyPassword(e.target.value)}
				/>
			</Form.Group>

			{isActive ? 
				<Button variant="primary" type="submit" className="mt-3">Register</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Register</Button>
			}

		</Form>
		)
}