import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';

export default function ProductPage() {

//store the data in a State allProducts
	const [ allProducts, setAllProducts ] = 
//[] -> getter is an empty array because we are anticipating an array of dataa from fetch
	useState([])
	
	const fetchData = () => {
		fetch('http://localhost:4000/products/allProducts')
		.then(res => res.json())
		.then(data => {
			console.log(data)
//store the all the data to the useState setAllProducts and pass it to the props productData to be use in theuserView
			setAllProducts(data)
		}) 
	}
//callback fetch for the data to render
	useEffect(() => {
		fetchData()
//[] (array is empty) -> data will be rendered once
	}, [])

	const { user } = useContext(UserContext);

	return(
			<>
				{(user.isAdmin === true) ? 
					<AdminView productsData={allProducts} fetchData={fetchData}/>
					:
					<UserView productsData={allProducts} />
//productData={allProducts} -> set the props to be used in the other file
				}				
			</>
		)
}