const productsData = [
	{
		id:"wdc001",
		name: "AIR JORDAN 1 MID SE ",
		price: 7000,
		description: "Color: Dutch Green, Sizes: 36-46"
	},
	{
		id:"wdc002",
		name: "NIKE AIR FORCE 1",
		price: 5500,
		description: "Color: White, Sizes: 36-46, Low(3M)"
	},
	{
		id:"wdc003",
		name: "ADIDAS ULTRABOOST 4.0",
		price: 6500,	
		description: "Color: White, Sizes: 36-46"
	},

]

export default productsData;