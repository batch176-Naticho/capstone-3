import { useState } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import ProductPage from './pages/ProductPage';
import OrderProduct from './pages/OrderProduct';
import {Container} from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { BrowserRouter, Routes, Route } from 'react-router-dom';



function App() {

//store and validate user information
  const [ user, setUser ] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  //clear localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value = {{ user, setUser, unsetUser }} >
      <BrowserRouter>
        <AppNavbar />
        <Container>        
          <Routes>
              <Route path="/" element={ <Home />} />
              <Route path="/products" element={ <ProductPage />} />
              <Route path="/register" element={ <Register />} />
              <Route path="/login" element={ <Login />} />
              <Route path="/logout" element={ <Logout />} />
              <Route path="*" element={<ErrorPage />} />      
          </Routes> 
        </Container>      
     </BrowserRouter>
   </UserProvider>
  );
}

export default App;

