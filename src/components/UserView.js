import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';


export default function UserView({productsData}) {

	const [ products, setProducts ] = useState([])
// .map to accomodate rapid changes in the data (add products or delete a product) 
	useEffect(() => {

		const productsArray = productsData.map(product => {
			if(product.isActive === true ){
				return(
					<ProductCard key={product._id} productProp={product}/>
					)
			}else{
				return null
			}
		})
//
	setProducts(productsArray)

	}, [productsData])	



	return(
		<>
			{products}
		</>
		)
}