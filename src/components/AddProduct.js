import { useState } from 'react';
import { Button, Form, Modal } from "react-bootstrap";
import Swal from 'sweetalert2'


export default function AddProduct({fetchData}) {

//state for the formof adding a product	
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState('');

//state for openning and closing the modals
	const [ showModal, setShowModal ] = useState(false)

//function to handle openning and closing of modal
	const openModal = () => setShowModal(true);
	const closeModal = () => setShowModal(false);


//function for adding product
const addProduct = (e) => {
	e.preventDefault();

	fetch('http://localhost:4000/products/product', {
		method: 'POST',
		headers:{'Content-Type': 'Application/json',
		Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
		},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(data){
			Swal.fire({
				title:'Success',
				icon:'success',
				text:'Product Successfully Added '
			})
//if adding product is successfull close the modal
		closeModal()

		fetchData()
		}else{
			Swal.fire({
				title:'Error',
				icon:'error',
				text:'Failed to add product '
			})
			fetchData()
		}
//reset all stste inputs to empty
	setName('')
	setDescription('')
	setPrice(0)
	})
}
	return(
	<>
		<Button variant="primary" onClick={openModal}>Add New Product</Button>
{/*Add Modal 
if the openModal is triggered modal will be shown, if closeModal is triggered onHide will be triggered
Modal.Header closeButton -> will show the X in the right side to close the modal
*/}
	<Modal show={showModal} onHide={closeModal} >
		<Form onSubmit={e => addProduct(e)}>
			<Modal.Header closeButton>
				<Modal.Title>Add a Product</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<Form.Group>
					<Form.Label>Name</Form.Label>
					<Form.Control
					      type="text"
					      required
					      value={name}
					      onChange={e => setName(e.target.value)}

					 />
				</Form.Group>

				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control
					      type="text"
					      required
					      value={description}
					      onChange={e => setDescription(e.target.value)}

					 />
				</Form.Group>

				<Form.Group>
					<Form.Label>Price</Form.Label>
					<Form.Control
					      type="number"
					      required
					      value={price}
					      onChange={e => setPrice(e.target.value)}
					 />
				</Form.Group>
			</Modal.Body>

			<Modal.Footer>
				<Button variant="secondary" onClick={closeModal}>Close</Button>
				<Button variant="success" type="submit">Add Product</Button>
			</Modal.Footer>
		</Form>
	</Modal>
	</>
		)
}

//props drilling
//we reuse fetchData in the product page for the added product to be rendered automatically, no need to refresh before the added product appear in the dashboard.
//ProdcutPage line  33 <AdminView productsData={allProducts} fetchData={fetchData}/>

//we add fetchData in the props (const { productsData, fetchData } = props;) and the <Addproduct (<AddProduct fetchData={fetchData}) of admin view and from admin view we pass it in the add product(export default function AddProduct(export default function AddProduct({fetchData}))
