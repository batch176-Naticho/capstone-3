import { Row, Col, Card, Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Highlight() {
	return (
<Row className="highlight-container">
	<Col xs={12} md={3}>
		<Card className="cardHighlight p-3 bg-secondary">
			<div className="img-container">
				<Card.Img variant="top" src="/nike22.png" />
			</div>
			<Card.Body>
				<Card.Title className="card1">Nike</Card.Title>
				<ListGroup>
					<ListGroupItem>Authentic</ListGroupItem>
					<ListGroupItem>Pred-Order</ListGroupItem>
					<ListGroupItem>For Adults and Kids</ListGroupItem>
					<ListGroupItem>Shipment Processing: 27-35 days</ListGroupItem>
					<ListGroupItem>Client must be willing to wait</ListGroupItem>
				</ListGroup>
				<Button variant="primary" className="mt-3" as={Link} to="/products">Browes</Button>
			</Card.Body>
		</Card>
	</Col>

	<Col xs={12} md={3}>
		<Card className="cardHighlight p-3 bg-secondary">
			<Card.Img variant="top" src="/ad1.png"  />
			<Card.Body>
				<Card.Title>Adidas</Card.Title>
				<ListGroup>
					<ListGroupItem>Authentic, For Adults </ListGroupItem>
					<ListGroupItem>Pred-Order</ListGroupItem>
					<ListGroupItem>Shipment Processing: 27-35 days</ListGroupItem>
					<ListGroupItem>Client must be willing to wait</ListGroupItem>
				</ListGroup>
				<Button variant="primary" className="mt-3">Browes</Button>
			</Card.Body>
		</Card>
	</Col>

	<Col xs={12} md={3}>
		<Card className="cardHighlight p-3 bg-secondary">
			<Card.Img variant="top" src="/vans11.jpg" />
			<Card.Body>
				<Card.Title>Vans</Card.Title>
				<ListGroup>
					
					<ListGroupItem>Authentic, For Adults </ListGroupItem>
					<ListGroupItem>Pred-Order</ListGroupItem>
					<ListGroupItem>Shipment Processing: 27-35 days</ListGroupItem>
					<ListGroupItem>Client must be willing to wait</ListGroupItem>
				</ListGroup>
				<Button variant="primary" className="mt-3">Browes</Button>
			</Card.Body>
		</Card>
	</Col>

	<Col xs={12} md={3}>
		<Card className="cardHighlight p-3 bg-secondary">
			<Card.Body>
				<Card.Header className="cardHeader">
					<h4>OTHER PRODUCTS</h4>
				</Card.Header>
				<Card.Title className="mt-3">Birkenstock</Card.Title>
				<Card.Title className="mb-3">Beltbags</Card.Title>
				<ListGroup>
					<ListGroupItem>Authentic</ListGroupItem>
					<ListGroupItem>For Adults </ListGroupItem>
					<ListGroupItem>Pred-Order</ListGroupItem>
					<ListGroupItem>Shipment Processing: 27-35 days</ListGroupItem>
					<ListGroupItem>Client must be willing to wait</ListGroupItem>
				</ListGroup>
				<Button variant="primary" className="mt-3">Browes</Button>
			</Card.Body>
		</Card>
	</Col>
</Row>
	)

}