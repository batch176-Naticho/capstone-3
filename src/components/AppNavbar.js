import { useState, useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	const {user} = useContext(UserContext)

	//const [user, setUser ] = useState(localStorage.getItem('email'))
	//console.log(user)

	return(
		<Navbar bg="warning" expand="lg" variant="dark" className="mb-5 sticky-top ">
			<Navbar.Brand className="ms-3 text-dark">S&S online</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav" >
				<Nav className="ms-auto navLinks" >
					<Nav.Link as={Link} to="/" className="text-dark">Home</Nav.Link>
					<Nav.Link as={Link} to="/products" className="text-dark">Our Products</Nav.Link>
				

					{(user.accessToken !== null) ?
						<Nav.Link as={Link} to="/logout" className=" text-dark" >Logout</Nav.Link>	
						:
						<>
						<Nav.Link as={Link} to="/login" className="text-dark">Login</Nav.Link>
						<Nav.Link as={Link} to="/register" className="text-dark">Register</Nav.Link>
						</>
					}
				</Nav>				
			</Navbar.Collapse>		

		</Navbar>
		)
}
