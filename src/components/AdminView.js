import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import AddProduct from './AddProduct';
import UpdateProduct from './UpdateProduct';
import ArchiveProduct from './ArchiveProduct';


export default function AdminView(props) {

	const { productsData, fetchData } = props;
	const [ products, setProducts] = useState([])
	useEffect(() => {
		const productsArray = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td>
						<UpdateProduct product={product._id} fetchData={fetchData}/>
					</td>
					<td>
						<ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/>
					</td>
					
				</tr>
				)
		})
		setProducts(productsArray)
	}, [productsData])


	return(
		<>
			<div className="text-center my-4">
				<h1>Admin Dashboard</h1>
				<AddProduct fetchData={fetchData}/>
				
			</div>
			
			<Table striped bordered hover responsive >
				<thead className="bg-secondary text-dark">
					<tr className="text-center">
						<th>ID</th>
						<th>NAME</th>
						<th>DESCRIPTION</th>
						<th>PRICE</th>
						<th>AVAILABILITY</th>
						<th ColSpan="2">ACTIONS</th>
					</tr>
				</thead>
				<tbody className="text-center">	
					{products}				
				</tbody>
			</Table>

		</>
		)
}