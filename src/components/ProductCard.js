import { useState } from "react";
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function ProductCard({productProp}) {
	//console.log(props);

	const { name, description, price } = productProp

	return(
		<Card className="mt-3">
			<Card.Body>
				<Card.Title>{ name }</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{ description }</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php { price }</Card.Text>
				<Button variant="primary">Buy</Button>
			</Card.Body>
		</Card>

		)
}

//PropTypes

ProductCard.propTypes = {
	productProp: PropTypes.shape({
	name: PropTypes.string.isRequired,	
	description: PropTypes.string.isRequired,
	price: PropTypes.number.isRequired
	})
}