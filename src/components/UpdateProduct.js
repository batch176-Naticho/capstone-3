import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2'


export default function UpdateProduct({product,
//product -> id, to get the other properties/information from the document
//fetchData -> rerendering
 	fetchData}) {
//states for edit product
	
	const [ showEdit, setShowEdit ] = useState(false)

//state hook for the product information
	const [ productId, setProductId ] = useState('');
	const [ name, setName ] = useState('');
	const [ description, setDescription] = useState('');
	const [ price, setPrice ] = useState(0)	;


//function to get the data from the form (admin dashboard) while opening the modal
	const openEdit = (productId) => {
		fetch(`http://localhost:4000/products/${ productId }`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)
//populate the input values with the product information that we fetch
//when the update button is trigered the modal will appear and this guy here will display the product infromation we wish to update
	setProductId(data._id)
	setName(data.name)
	setDescription(data.description)
	setPrice(data.price)

		})
		setShowEdit(true)
	}

//function to handle the closing of modal and reset all states to their default value
	const closeEdit = () => {
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice(0)
	}

//function to update/edit product
	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`http://localhost:4000/products/${ productId }`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name:name,
				description:description,
				price:price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title:'success',
					icon: 'success',
					text: 'Product Updated'
				})
				fetchData()
				closeEdit()
			}else{
				Swal.fire({
					title: 'error',
					icon: 'error',
					tetx: 'Failed to Update Product'	
				})
				fetchData()
				closeEdit()
			}
		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}>Update</Button>
{/*Edit Modal*/}
				<Modal show={showEdit} onHide={closeEdit}>
					<Form onSubmit={e => editProduct(e,productId)}>
						<Modal.Header closeButton>
							<Modal.Title>Edit a Product</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<Form.Group>
								<Form.Label>Name</Form.Label>
								<Form.Control
								      type="text"
								      required
								      value={name}
								      onChange={e => setName(e.target.value)}

								 />
							</Form.Group>

							<Form.Group>
								<Form.Label>Description</Form.Label>
								<Form.Control
								      type="text"
								      required
								      value={description}
								      onChange={e => setDescription(e.target.value)}

								 />
							</Form.Group>

							<Form.Group>
								<Form.Label>Price</Form.Label>
								<Form.Control
								      type="number"
								      required
								      value={price}
								      onChange={e => setPrice(e.target.value)}
								 />
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							<Button variant="secondary" onClick={closeEdit}>Close</Button>
							<Button variant="success" type="submit">Update Product</Button>
						</Modal.Footer>
					</Form>
				</Modal>
		</>
		)
}